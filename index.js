var express = require("express"),
    handlebars = require('express-handlebars');
 
var bodyParser = require('body-parser');
 
var app = express();
 
 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            }
            ];
 
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
           
           
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 
 //Enseña los posts
 
app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});
 
 //añade un post
app.post('/posts', function(req, res){
 
    posts.push(req.body);
 
    res.end(JSON.stringify(req.body));
});

//muestra el ultimo post
app.get('/posts/new', function(req, res){
    res.render('posts', { "title": "novedades", "posts" : [posts[posts.length -1]] } );
});


app.get('/posts/:id', function(req, res){
	var user_id = req.id;
	res.render('posts', { "title": "Posts by id", "posts" : [posts[user_id]] } );
    //coge el post en la posicion id y lo muestra
});

//edit edita el ultimo post
app.get("/posts/edit", function(req, res) {
	posts[posts.length-1] = req.body;
	res.render('posts', { "title": "Edit Post", "posts" : [posts[posts.length-1]] } );
});

//put: modifica el post con index id
app.put("/posts/:id", function(req, res) {
	posts[req.params.id] = req.body;
	res.render('posts', { "title": "El post se ha editado correctamente", "posts" : [posts[req.params.id]] } );

});

//del borra el post con posicion id
app.delete("/posts/:id", function(req, res) {
		posts.splice(req.params.id,1);
	 	res.render('posts', { "title": "Post deleted"} );

});

 
app.listen(8080);